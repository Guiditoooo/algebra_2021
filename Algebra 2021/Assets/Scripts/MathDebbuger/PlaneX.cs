﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace CustomMath {
    public struct PlaneX
    {
        private Vec3 norm; //Normal
        private float dist; // El plano responde a la funcion AX + BY + CZ + D = 0. D = dist. 
                            // Indica el defasaje del plano, sobre la normal para arriba o abajo, dependiendo de su signo.
        public PlaneX(Vec3 n, Vec3 p) //El plano tiene normal n y pasa por el punto p.
        {
            norm = Vec3.Normalize(n);
            dist = Vec3.Dot(n, p);
        }
        public PlaneX(Vec3 n, float d) //Distancia minima respecto al eje de coordenadas
        {
            norm = Vec3.Normalize(n);
            dist = d;
        }
        public PlaneX(Vec3 a, Vec3 b, Vec3 c)//El plano pasa por estos 3 puntos. La normal se hace con a y b.
        {
            Vec3 v1 = b - a, v2 = c - a;
            Vec3 aux = Vec3.Normalize(Vec3.Cross(v1, v2));
            
            norm = aux;
            dist = Vec3.Dot(norm, a);
        }
        public Vec3 normal
        {
            get { return norm; }
            set { norm = value; }
        }
        public float distance
        {
            get { return dist; }
            set { dist = value; }
        }
        public PlaneX flipped //devuelve el plano dado vuelta.
        {
            get { return new PlaneX(-norm, -dist); }
        }
        public static PlaneX Translate(PlaneX plane, Vec3 move) //Defasa el plano una cantidad move. Usa la tangente como carril.
        {
            return new PlaneX(plane.normal, plane.distance + Vec3.Dot(plane.normal, move));
        }
        public Vec3 ClosestPointOnPlane(Vec3 p) //Devuelve un punto perteneciente al plano, que está más cerca del punto p.
        {
            float aux = Vec3.Dot(norm, p) + dist;
            return (p - norm * aux);
        }
        public void Flip() //Da vuelta el plano
        {
            norm = -norm;
            dist = -dist;
        }
        public float GetDistanceToPoint(Vec3 p)
        {
            return (Vec3.Dot(norm, p) + dist); //dist entre normal y p + corrimiento agregado d
        }
        public bool GetSide(Vec3 p) //De que lado del plano esta el punto ingresado
        {
            return Vec3.Dot(norm, p) + dist > 0.0f;
            
        }
        public bool Raycast(Ray ray, out float enter) //Devuelve la distancia entre el rayo y el plano
        {
            Vec3 auxNum = new Vec3(ray.origin.x, ray.origin.y, ray.origin.z); //Inicio del Rayo
            Vec3 auxDen = new Vec3(ray.direction.x, ray.direction.y, ray.direction.z); //Fin del rayo
            float num = Vec3.Dot(auxNum, norm) + dist; //Encuentra la distancia del rayo respecto de la normal del plano, y la multiplica por la distancia al Origen
            float den = Vec3.Dot(auxDen, norm); //Encuentras la distancia del rayo respecto de la normal.

            if (Mathf.Approximately(den, 0.0f)) //Si el denominador es parecido a 0 implica que el rayo es paralelo al plano
            {
                enter = 0.0f; 
                return false;
            }

            enter = -num / den;
            return enter > 0.0; //Si el rayo esta del lado positivo o negativo
        }
        public bool SameSide(Vec3 inPt0, Vec3 inPt1) //Se fija si los 2 puntos estan del mismo lado del plano
        {
            float distanceToPoint1 = GetDistanceToPoint(inPt0);
            float distanceToPoint2 = GetDistanceToPoint(inPt1);
            return distanceToPoint1 > 0.0 && distanceToPoint2 > 0.0 || distanceToPoint1 <= 0.0 && distanceToPoint2 <= 0.0;
        }
        public void Set3Points(Vec3 a, Vec3 b, Vec3 c) //Practicamente igual al constructor con 3 puntos.
        {
            norm = Vec3.Normalize(Vec3.Cross(b - a, c - a));
            dist = -Vec3.Dot(norm, a);
        }
        public void SetNormalAndPosition(Vec3 normal, Vec3 p) //Igual al constructor de la normal y el punto
        {
            norm = Vec3.Normalize(normal);
            dist = -Vec3.Dot(norm, p);
        }
        public override string ToString()
        {
            return "No Sabe, no contesta";
        }
        public string ToString(string format)
        {
            return "No Sabe, no contesta";
        }
        public string ToString(string format, IFormatProvider formatProvider)
        {
            return "No Sabe, no contesta";
        }
        public void Translate(Vec3 move)
        {
            dist = dist + Vec3.Dot(norm, move);
        }
        

    }
}
