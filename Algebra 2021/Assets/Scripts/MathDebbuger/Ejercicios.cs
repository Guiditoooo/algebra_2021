﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;

public class Ejercicios : MonoBehaviour
{
    public enum Ejercicio
    {
        Uno,
        Dos,
        Tres,
        Cuatro,
        Cinco,
        Seis,
        Siete,
        Ocho,
        Nueve,
        Diez
    }

    public Ejercicio ejercicio = Ejercicio.Uno;
    public Color VectorColor = Color.green;
    public Vector3 A;
    public Vector3 B;
    private Vector3 C;

    //private bool done = false;

    private float time = 0;
    private const int timeLimit = 10;

    void Start()
    {
        VectorDebugger.AddVector(A, Color.white, "0"); //Agrega un vector al grupo "0"
        VectorDebugger.AddVector(B, Color.black, "1"); //Agrega un vector al grupo "1"
        VectorDebugger.AddVector(Vector3.zero, VectorColor, "2"); //Agrega un vector al grupo "2"

        VectorDebugger.EnableEditorView(); //Habilita la visualización en el editor, si no, solo en ejecución
        VectorDebugger.EnableCoordinates(); //Muestra las coordenadas abajo de los puntos de los vectores.
    }

    void Update()
    {

        VectorDebugger.UpdatePosition("0", A); //Actualizar el vector del grupo "0" segun se elija en el inspector
        VectorDebugger.UpdatePosition("1", B);//Actualizar el vector del grupo "1" segun se elija en el inspector

        switch (ejercicio)
        {
            case Ejercicio.Uno:
                C = A + B; //Uno mas el otro
                break;
            case Ejercicio.Dos:
                C = B - A; //El segundo menos el primero
                break;
            case Ejercicio.Tres:
                C = new Vector3(A.x * B.x, A.y * B.y, A.z * B.z); //Multiplicacion componente a componente
                break;
            case Ejercicio.Cuatro:
                C = Vector3.Cross(B,A); //Equivalente a -Vector3.Cross(A,B). Producto cruz entre ambos vectores
                break;                  //Obtengo el vector ortogonal a ambos vectores al mismo tiempo
            case Ejercicio.Cinco:
                time = time > 1 ? 0 : time + Time.deltaTime; //Si el tiempo no es 0, suma.
                C = Vector3.Lerp(A, B, time);                //Si el tiempo es 1, se resetea (a 0).
                break;
            case Ejercicio.Seis:
                C = Vector3.Max(A, B);  //Agarra el mayor valor entre las componentes de ambos vectores.
                break;
            case Ejercicio.Siete:
                C = Vector3.Project(A, B);
                break;
            case Ejercicio.Ocho:
                C = (A + B).normalized * Vector3.Distance(A, B);
                break;
            case Ejercicio.Nueve:
                C = Vector3.Reflect(A, B.normalized);
                break;
            case Ejercicio.Diez:
                time = time > timeLimit ? 0 : time + Time.deltaTime; //Si el tiempo no es 0, suma.
                C = Vector3.LerpUnclamped(B, A, time);
                break;
            default:
                break;
        }

        VectorDebugger.UpdatePosition("2", C); //Actualizar el vector del grupo "2" dependiendo del ejercicio seleccionado en el inspector

        

    }


}
