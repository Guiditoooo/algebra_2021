﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace CustomMath
{
    public struct Vec3 : IEquatable<Vec3>
    {
        #region Variables
        public float _x;
        public float _y;
        public float _z;

        public float sqrMagnitude { get { return SqrMagnitude(new Vec3(_x, _y, _z)); } }
        public Vec3 normalized { get { Vec3 j = new Vec3(_x, _y, _z); return (Normalize(j)); } }
        public float magnitude { get { return Magnitude(new Vec3(_x, _y, _z)); } }
        #endregion

        #region constants
        public const float epsilon = 1e-05f;
        #endregion

        #region Default Values
        public static Vec3 zero { get { return new Vec3(0, 0, 0); } }
        public static Vec3 one { get { return new Vec3(1, 1, 1); } }
        public static Vec3 forward { get { return new Vec3(0, 0, 1); } }
        public static Vec3 back { get { return new Vec3(0, 0, -1); } }
        public static Vec3 right { get { return new Vec3(1, 0, 0); } }
        public static Vec3 left { get { return new Vec3(-1, 0, 0); } }
        public static Vec3 up { get { return new Vec3(0, 1, 0); } }
        public static Vec3 down { get { return new Vec3(0, -1, 0); } }
        public static Vec3 positiveInfinity { get { return new Vec3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity); } }
        public static Vec3 negativeInfinity { get { return new Vec3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity); } }
        #endregion                                                                                                                                                                               

        #region Constructors
        //public Vec3()
        //{
        //    _x = 0;
        //    _y = 0;
        //    _z = 0;
        //}
        public Vec3(float x, float y)
        {
            _x = x;
            _y = y;
            _z = 0.0f;
        }
        public Vec3(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }
        public Vec3(Vec3 v3)
        {
            _x = v3._x;
            _y = v3._y;
            _z = v3._z;
        }
        public Vec3(Vector3 v3)
        {
            _x = v3.x;
            _y = v3.y;
            _z = v3.z;
        }
        public Vec3(Vector2 v2)
        {
            _x = v2.x;
            _y = v2.y;
            _z = 0.0f;
        }
        #endregion

        #region Operators
        public static bool operator ==(Vec3 a, Vec3 b) //Si las 3 componentes son iguales
        {
            float diff_x = a._x - b._x;
            float diff_y = a._y - b._y;
            float diff_z = a._z - b._z;
            float total = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
            return total < Mathf.Epsilon; //epsilon es muy muy chico, 
        }
        public static bool operator !=(Vec3 left, Vec3 right)
        {
            return !(left == right);
        }
        public static Vec3 operator +(Vec3 a, Vec3 b)
        {
            return new Vec3(a._x + b._x, a._y + b._y, a._z + b._z);
        }
        public static Vec3 operator -(Vec3 a, Vec3 b)
        {
            return new Vec3(a._x - b._x, a._y - b._y, a._z - b._z);
        }
        public static Vec3 operator -(Vec3 v3)
        {
            return new Vec3(-v3._x, -v3._y, -v3._z);
        }
        public static Vec3 operator *(Vec3 v3, float scalar)
        {
            return new Vec3(v3._x * scalar, v3._y * scalar, v3._z * scalar);
        }
        public static Vec3 operator *(float scalar, Vec3 v3)
        {
            return new Vec3(v3._x * scalar, v3._y * scalar, v3._z * scalar);
        }
        public static Vec3 operator /(Vec3 v3, float scalar)
        {
            return new Vec3(v3._x / scalar, v3._y / scalar, v3._z / scalar);
        }

        public static implicit operator Vector3(Vec3 v3)
        {
            return new Vector3(v3._x, v3._y, v3._z);
        }

        public static implicit operator Vec3(Vector3 v3)
        {
            return new Vec3(v3.x, v3.y, v3.z);
        }

        public static implicit operator Vector2(Vec3 v2)
        {
            return new Vector2(v2._x, v2._y);
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "X = " + _x.ToString() + "   Y = " + _y.ToString() + "   Z = " + _z.ToString();
        }
        public static float Angle(Vec3 from, Vec3 to) //Devuelve el angulo formado entre from y to, por medio de hacer algo parecido a pitagoras
        {
            return Mathf.Acos(Mathf.Sqrt(Mathf.Pow(to._x + from._y, 2) + Mathf.Pow(to._y + from._y, 2) + Mathf.Pow(to._z + from._z, 2))) * Mathf.Rad2Deg;
        }
        public static Vec3 ClampMagnitude(Vec3 vector, float maxLength) //Acortar la magnitud (fija un limite)
        {
            return new Vec3((vector / Magnitude(vector)) * maxLength);
        }
        public static float Magnitude(Vec3 vector) // Magnitud: Raiz cuadrada de la suma de los cuadrados de las componentes
        {
            return Mathf.Sqrt(Mathf.Pow(vector._x,2) + Mathf.Pow(vector._y, 2) + Mathf.Pow(vector._z, 2));
        }
        public static Vec3 Cross(Vec3 a, Vec3 b) // producto cruz
        {
            //multiplico las componentes cruzadas que no son la que estoy buscando, y luego, las resto. 
            return new Vec3((a._y * b._z - a._z *b._y), (a._z * b._x - a._x * b._z), (a._x * b._y - a._y * b._x));
        }
        public static float Distance(Vec3 a, Vec3 b) // distancia
        {
            return Mathf.Sqrt(Mathf.Pow(a._x - b._x, 2) + Mathf.Pow(a._y - b._y, 2)+ Mathf.Pow(a._z - b._z, 2));
        }
        public static float Dot(Vec3 a, Vec3 b) //producto punto componente a componente
        {
            return a._x * b._x + a._y * b._y + a._z * b._z;
        }
        public static Vec3 Lerp(Vec3 a, Vec3 b, float t) //ir de un vector al otro
        {
            Vec3 j = zero; //Hace un calculo componente a componente en la que chequea a que distancia debe estar entre a y b, dependiendo de t
            if (t < 1 && t>0) { // Lerp va desde 0 a 1. Si no entrega vec3.zero //ACA HABIA UN ERROR :D
                j._x = a._x * (1.0f - t) + (b._x * t);
                j._y = a._y * (1.0f - t) + (b._y * t);
                j._z = a._z * (1.0f - t) + (b._z * t);
            }
            return j;
        }
        public static Vec3 LerpUnclamped(Vec3 a, Vec3 b, float t) 
        {
            Vec3 j = new Vec3(); //Hace un calculo componente a componente en la que chequea a que distancia debe estar entre a y b, dependiendo de t. Infinito
                j._x = a._x * (1.0f - t) + (b._x * t);
                j._y = a._y * (1.0f - t) + (b._y * t);
                j._z = a._z * (1.0f - t) + (b._z * t);
            return j;
        }
        public static Vec3 Max(Vec3 a, Vec3 b) //Engrega un vector con los mas altos valores de x, y & z de los 2 vectores ingresados
        {
            Vec3 j = new Vec3();
            j._x = a._x >= b._x? a._x : b._x;
            j._y = a._y >= b._y? a._y : b._y;
            j._z = a._z >= b._z? a._z : b._z;
            return j;
        }
        public static Vec3 Min(Vec3 a, Vec3 b) //Engrega un vector con los mas bajos valores de x, y & z de los 2 vectores ingresados
        {
            Vec3 j = new Vec3();
            j._x = a._x <= b._x ? a._x : b._x;
            j._y = a._y <= b._y ? a._y : b._y;
            j._z = a._z <= b._z ? a._z : b._z;
            return j;
        }
        public static float SqrMagnitude(Vec3 v) //Raiz de la magnitud
        {
            return Mathf.Pow(v._x, 2) + Mathf.Pow(v._y, 2) + Mathf.Pow(v._z, 2);
        }
        public static Vec3 Project(Vec3 v, Vec3 norm) //Hay fotito
        {
            return new Vec3 ((Dot(v, norm) / (norm.magnitude * norm.magnitude)) * norm);
        }
        public static Vec3 Reflect(Vec3 dir, Vec3 norm) 
        {
            norm = Normalize(norm);
            return new Vec3(dir - 2 * Project(dir, norm));
        }
        public void Set(float newX, float newY, float newZ) 
        {
            _x = newX;
            _y = newY;
            _z = newZ;
        }
        public void Scale(Vec3 scale) 
        {
            _x *= scale._x;
            _y *= scale._y;
            _z *= scale._z;
        }
        public static Vec3 Normalize(Vec3 v)
        {
            float j = Magnitude(v);
            float _x, _y, _z;
            _x = v._x / j;
            _y = v._y / j;
            _z = v._z / j;
            return new Vec3(_x,_y,_z);
        }
        public Vec3 Vector3ToVec3(Vector3 v)
        {
            Vec3 aux = zero;
            aux._x = v.x;
            aux._y = v.y;
            aux._z = v.z;
            return aux;
        }
        #endregion

        #region Internals
        public override bool Equals(object other)
        {
            if (!(other is Vec3)) return false;
            return Equals((Vec3)other);
        }
        public bool Equals(Vec3 other)
        {
            return _x == other._x && _y == other._y && _z == other._z;
        }
        public override int GetHashCode()
        {
            return _x.GetHashCode() ^ (_y.GetHashCode() << 2) ^ (_z.GetHashCode() >> 2);
        }
        #endregion
    }
}