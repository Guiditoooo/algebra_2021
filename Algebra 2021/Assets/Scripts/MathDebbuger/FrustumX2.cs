﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using CustomMath;

public class FrustumX2 : MonoBehaviour
{
    
    private Camera cam;
    enum PLANES { TOP, BOT, FRONT, BACK, LEFT, RIGHT }

    private GameObject[] lim;
    private Plane[] planes = new Plane[6];
    private GameObject[] objectList;
    private List<MeshFilter> meshList = new List<MeshFilter>();
    private List<MeshRenderer> renderList = new List<MeshRenderer>();
    private Vector3[] vertices;
    private Vector3 center = Vector3.zero;

    void Awake()
    {
        objectList = GameObject.FindGameObjectsWithTag("Showeable");

        foreach (GameObject go in objectList)
        {
            meshList.Add(go.GetComponent<MeshFilter>());
            renderList.Add(go.GetComponent<MeshRenderer>());
        }

    }

    void Start()
    {
        for (int i = 0; i < lim.Length; i++)
        {
            lim[i].transform.rotation = Quaternion.identity;
            lim[i].transform.localScale = Vector3.one;
        }
    }

    void Update()
    {

        CalculateFrustumPoints();
        for (int j = 0; j < meshList.Count; j++)
        {
            renderList[j].enabled = CalculateObject(j);
        }
            DrawPlaneBoxLines();

    }

    void DrawPlaneBoxLines()
    {
        for (int i = 0; i < lim.Length; i++)
        {
            for (int j = 0; j < lim.Length; j++)
            {
                Debug.DrawLine(lim[i].transform.position, lim[j].transform.position, Color.green);
            }
        }
    }

    void DrawObjectBoxLines(Vector3[] v)
    {
        for (int i = 0; i < v.Length; i++)
        {
            for (int j = 0; j < v.Length; j++)
            {
                Debug.DrawLine(v[i], v[j], Color.red);
            }
        }
    }

    void CalculateFrustumPoints()
    {
        cam = Camera.main;

        float near = cam.nearClipPlane; //Distancia al plano más cercano
        float far = cam.farClipPlane; //Distancia al plano más lejano
        float aspect = cam.aspect; //Relacion entre ancho y alto
        float fovV = cam.fieldOfView;//En este caso, la camara esta seteada en vertical. Para otro caso hay que cambiar calculos
        //Tg = op / ad. op = altura y ad = near  
        float height = Mathf.Tan((fovV / 2) * Mathf.Deg2Rad) * near; //Mitad de arriba, y lo paso a radianes
        float width = (cam.aspect * height);
        Vector3 pos = cam.transform.position;

        //height *= cam.transform.up.y;
        //width *= cam.transform.right.x;
        //near *= cam.transform.forward.z;

        lim[0].transform.position = new Vector3(-width, -height, near) + pos;
        lim[1].transform.position = new Vector3(width, -height, near) + pos;
        lim[2].transform.position = new Vector3(-width, height, near) + pos;
        lim[3].transform.position = new Vector3(width, height, near) + pos;

        float posXN = (lim[0].transform.position.x + lim[1].transform.position.x)/2;
        float posYN = (lim[2].transform.position.y + lim[3].transform.position.y)/2;
        float posZN = (near + far) / 2;

        Vector3 centerN = new Vector3(posXN, posYN, posZN);

        height = Mathf.Tan((fovV / 2) * Mathf.Deg2Rad) * far;
        width = (aspect * height);

        //height *= cam.transform.up.y;
        //width *= cam.transform.right.x;
        //far *= cam.transform.forward.z;

        lim[4].transform.position = new Vector3(-width, -height, far) + pos;
        lim[5].transform.position = new Vector3(width, -height, far) + pos;
        lim[6].transform.position = new Vector3(-width, height, far) + pos;
        lim[7].transform.position = new Vector3(width, height, far) + pos;

        float posXF = (lim[4].transform.position.x + lim[5].transform.position.x) / 2;
        float posYF = (lim[6].transform.position.y + lim[7].transform.position.y) / 2;
        float posZF = (near + far) / 2;

        Vector3 centerF = new Vector3(posXF, posYF, posZF);

        foreach (GameObject GO in lim)
        {
            GO.transform.position = cam.transform.rotation * GO.transform.position;
        }

        planes[(int)PLANES.TOP]   = new Plane(lim[1].transform.position, lim[3].transform.position, lim[7].transform.position); //Uso 3 puntos que estan contenidos en este plano
        planes[(int)PLANES.BOT]   = new Plane(lim[0].transform.position, lim[2].transform.position, lim[6].transform.position);
        planes[(int)PLANES.FRONT] = new Plane(lim[0].transform.position, lim[4].transform.position, lim[5].transform.position);
        planes[(int)PLANES.BACK]  = new Plane(lim[2].transform.position, lim[3].transform.position, lim[7].transform.position);
        planes[(int)PLANES.LEFT]  = new Plane(lim[0].transform.position, lim[2].transform.position, lim[3].transform.position);
        planes[(int)PLANES.RIGHT] = new Plane(lim[5].transform.position, lim[7].transform.position, lim[6].transform.position);

        center = new Vector3(pos.x, pos.y, pos.z + (far + near) / 2); // Agarra el centro del frustum

        center = (centerF + centerN) / 2 + pos;

        //bool isInside = true;
        //planes[0].Flip(); //Correccion para que todos los planos miren al centro
        //planes[4].Flip();
        //planes[5].Flip();

        //Debug.Log(center.ToString());

        for (int i = 0; i < planes.Length; i++) //Esto agarra los planos que estan dados vuelta y los pone bien xD
        {
            if (!planes[i].GetSide(center))
            {
                planes[i].Flip();
            }
        }

    }

    bool CalculateObject(int j)
    {


        bool atLeastOneInside;

        vertices = new Vector3[meshList[j].sharedMesh.vertices.Length];

        for (int i = 0; i < meshList[j].sharedMesh.vertices.Length; i++)
        {
            vertices[i] = meshList[j].sharedMesh.vertices[i];
        }

        for (int i = 0; i < vertices.Length; i++)
        {
            //Vector3 aux = new Vector3(meshList[j].transform.TransformPoint(vertices[i])); //Hace que el transform de ese objeto sea global, _y no local. Por eso no rotaba con el objeto.
            Vector3 aux = meshList[j].transform.TransformPoint(vertices[i]);
            int _x = 0;
            bool isInside = true;
            foreach (Plane plane in planes)
            {

                if (isInside)
                {
                    Debug.Log(_x + " " + plane.GetSide(aux));
                    isInside = plane.GetSide(aux);
                }
                _x++;
            }
            if (isInside)
            {
                atLeastOneInside = true;
                return true;

            }

        }
        return false;

        //DrawObjectBoxLines(edges);

    }

}
