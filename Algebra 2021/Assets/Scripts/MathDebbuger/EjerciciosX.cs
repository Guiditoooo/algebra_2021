﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;
using CustomMath;

public class EjerciciosX : MonoBehaviour
{
    public enum Ejercicio
    {
        Uno,
        Dos,
        Tres,
        Cuatro,
        Cinco,
        Seis,
        Siete,
        Ocho,
        Nueve,
        Diez
    }

    public Ejercicio ejercicio = Ejercicio.Uno;
    public Color VectorColor = Color.green;
    public Vector3 A;
    public Vector3 B;
    private Vec3 C;
    private Vec3 vA;
    private Vec3 vB;

    //private bool done = false;

    private float time = 0;
    private const int timeLimit = 10;

    void Start()
    {

        VectorDebugger.AddVector(A, Color.white, "A"); //Agrega un vector al grupo "A"
        VectorDebugger.AddVector(B, Color.black, "B"); //Agrega un vector al grupo "B"
        VectorDebugger.AddVector(Vec3.zero, VectorColor, "C"); //Agrega un vector al grupo "C"

        VectorDebugger.EnableEditorView(); //Habilita la visualización en el editor, si no, solo en ejecución
        VectorDebugger.EnableCoordinates(); //Muestra las coordenadas abajo de los puntos de los vectores.
    }

    void Update()
    {

        vA = new Vec3(A);
        vB = new Vec3(B);

        VectorDebugger.UpdatePosition("A", Vec3ToVector3(vA)); //Actualizar el vector del grupo "A" segun se elija en el inspector
        VectorDebugger.UpdatePosition("B", Vec3ToVector3(vB));//Actualizar el vector del grupo "B" segun se elija en el inspector

        switch (ejercicio)
        {
            case Ejercicio.Uno:
                C = vA + vB; //Uno mas el otro
                break;
            case Ejercicio.Dos:
                C = vB - vA; //El segundo menos el primero
                break;
            case Ejercicio.Tres:
                C = new Vec3(vA._x * vB._x, vA._y * vB._y, vA._z * vB._z); //Multiplicacion componente a componente
                break;
            case Ejercicio.Cuatro:
                C = Vec3.Cross(vB, vA); //Equivalente a -Vec3.Cross(vA,vB). Producto cruz entre ambos vectores
                break;                  //Obtengo el vector ortogonal a ambos vectores al mismo tiempo
            case Ejercicio.Cinco:
                time = time > 1 ? 0 : time + Time.deltaTime; //Si el tiempo no es 0, suma.
                C = Vec3.Lerp(vA, vB, time);                //Si el tiempo es 1, se resetea (a 0).
                break;
            case Ejercicio.Seis:
                C = Vec3.Max(vA, vB);  //Agarra el mayor valor entre las componentes de ambos vectores.
                break;
            case Ejercicio.Siete:
                C = Vec3.Project(vA, vB);
                break;
            case Ejercicio.Ocho:
                C = (vA + vB).normalized * Vec3.Distance(vA, vB);
                break;
            case Ejercicio.Nueve:
                C = Vec3.Reflect(vA, vB.normalized);
                break;
            case Ejercicio.Diez:
                time = time > timeLimit ? 0 : time + Time.deltaTime; //Si el tiempo no es 0, suma.
                C = Vec3.LerpUnclamped(vB, vA, time);
                break;
            default:
                break;
        }

        VectorDebugger.UpdatePosition("C", Vec3ToVector3(C)); //Actualizar el vector del grupo "C" dependiendo del ejercicio seleccionado en el inspector

    }

    Vector3 Vec3ToVector3(Vec3 v) 
    {
        return new Vector3(v._x, v._y, v._z);
    }


}
