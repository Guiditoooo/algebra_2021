﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EjerciciosAlgebra;
public class FrustumX : MonoBehaviour
{

    private Camera cam;
    enum PLANES { TOP, BOT, FRONT, BACK, LEFT, RIGHT }

    private Vector3[] lim = new Vector3[8];
    private Plane[] planes = new Plane[6];
    private GameObject[] objectList;
    private List<MeshFilter> meshList = new List<MeshFilter>();
    private List<MeshRenderer> renderList = new List<MeshRenderer>();
    private Vector3[] vertices;
    private Vector3 center = Vector3.zero;

    void Awake()
    {
        objectList = GameObject.FindGameObjectsWithTag("Showeable"); //Tomo todos los objetos que se PUEDEN ocultar

        foreach (GameObject go in objectList)
        {
            meshList.Add(go.GetComponent<MeshFilter>());
            renderList.Add(go.GetComponent<MeshRenderer>());
        }
    }

    private void Start()
    {

    }

    void Update()
    {

        CalculateFrustumPoints();

        for (int j = 0; j < meshList.Count; j++) //Para cada objeto 
        {
            bool atLeastOneInside = false;
            //*llora*
            vertices = meshList[j].sharedMesh.vertices; //Acá si uso la palabra vértice (aunque debería ser vertex, para no hacer spanglish)

            for (int i = 0; i < vertices.Length; i++) //Para todos los vértices
            {
                vertices[i] = meshList[j].transform.TransformPoint(vertices[i]); //Hace que el transform de ese objeto sea global, y no local.
                int x = 0;
                bool isInside = true;
                foreach (Plane plane in planes) //Por cada plano del frustum
                {
                    
                    if (isInside) //Como isInside empieza en verdadero, solo basta con que uno de los planos no contenga el vértice, para que no esté adentro
                    {
                        //Debug.Log(x + " " + plane.GetSide(edges[i]));
                        isInside = plane.GetSide(vertices[i]);
                        
                    }
                    x++;

                }
                if (isInside) //En caso de estar adentro de los 6 planos del frustum al mismo tiempo
                {             //Como estoy hablando de los vértices de un objeto, basta que aunque sea un vértice esté, para considerar todo el objeto dentro
                    atLeastOneInside = true;
                    break;
                }

            }

            renderList[j].enabled = atLeastOneInside; //Voy a mostrar ese objeto ni bien, isInside de true en al menos un vértice del objeto.
            //DrawObjectBoxLines(edges);
        }

        DrawPlaneBoxLines();

    }

    void DrawPlaneBoxLines() 
    {
        for (int i = 0; i < lim.Length; i++)
        {
            for (int j = 0; j < lim.Length; j++)
            {
                Debug.DrawLine(lim[i], lim[j], Color.green);
            }
        }
    }

    void DrawObjectBoxLines(Vector3[] v)
    {
        for (int i = 0; i < v.Length; i++)
        {
            for (int j = 0; j < v.Length; j++)
            {
                Debug.DrawLine(v[i], v[j], Color.red);
            }
        }
    }

    void CalculateFrustumPoints()
    {
        cam = Camera.main; 

        float near   = cam.nearClipPlane;   //Distancia al plano más cercano
        float far    = cam.farClipPlane;    //Distancia al plano más lejano
        float aspect = cam.aspect;          //Relacion entre ancho y alto
        float fovV   = cam.fieldOfView;     //En este caso, la camara esta seteada en vertical. Para otro caso hay que cambiar calculos
        //Tg = op / ad. op = altura y ad = near  
        float height = Mathf.Tan((fovV / 2) * Mathf.Deg2Rad) * near; //Mitad de arriba, y lo paso a radianes 
        float width  = aspect * height; //Esta y la de arriba son para calcular el plano near
        Vector3 pos  = cam.transform.position; //posición de la camara

        lim[0] = new Vector3(-width, -height, near) + pos; //abajo izquierda
        lim[1] = new Vector3(width, -height, near)  + pos; //abajo derecha
        lim[2] = new Vector3(-width, height, near)  + pos; //arriba izquierda
        lim[3] = new Vector3(width, height, near)   + pos; //arriba derecha


        height = Mathf.Tan((fovV / 2) * Mathf.Deg2Rad) * far; //Mas sohcahtoa por acá
        width  = aspect * height; //Esta y la de arriba son para calcular el plano far

        lim[4] = new Vector3(-width, -height, far) + pos; //abajo izquierda
        lim[5] = new Vector3(width, -height, far)  + pos; //abajo derecha
        lim[6] = new Vector3(-width, height, far)  + pos; //arriba izquierda
        lim[7] = new Vector3(width, height, far)   + pos; //arriba derecha


        for (int i = 0; i < lim.Length; i++) //Roto todos los puntos con la rotación de la cámara
        {    
            lim[i] = cam.transform.rotation * lim[i];
        }

        planes[(int)PLANES.TOP]   = new Plane(lim[1], lim[3], lim[7]); //Uso 3 puntos que estan contenidos en este plano
        planes[(int)PLANES.BOT]   = new Plane(lim[0], lim[2], lim[6]); //Hay fotito de esto
        planes[(int)PLANES.FRONT] = new Plane(lim[0], lim[4], lim[5]);
        planes[(int)PLANES.BACK]  = new Plane(lim[2], lim[3], lim[7]);
        planes[(int)PLANES.LEFT]  = new Plane(lim[0], lim[2], lim[3]);
        planes[(int)PLANES.RIGHT] = new Plane(lim[5], lim[7], lim[6]);

        center = new Vector3(pos.x, pos.y, pos.z + (far + near)/2); // Agarra el centro del frustum
        center = cam.transform.rotation * center; //Aplica la misma rotación de la cámara al centro del frustum
        //center = (centerF + centerN) / 2 + pos;

        //bool isInside = true;
        //planes[0].Flip(); //Correccion para que todos los planos miren al centro
        //planes[4].Flip();
        //planes[5].Flip();

        //Debug.Log(center.ToString());

        for (int i = 0; i < planes.Length; i++) //Esto agarra los planos que estan dados vuelta y los pone bien xD
        { 
            if (!planes[i].GetSide(center))
            {
                planes[i].Flip();
            }
        }

    }

}
