﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEnter : MonoBehaviour
{

    public GameObject col;
    private MeshFilter mesh;

    enum PLANES { TOP, BOT, FRONT, BACK, LEFT, RIGHT }

    const int tam = 3;
    private Vector3[] lim = new Vector3[8];
    private Plane[] planes = new Plane[6];
    Vector3[] edges;
    void Awake()
    {

        lim[0] = new Vector3(-tam, -tam, -tam);
        lim[1] = new Vector3(-tam, -tam, tam);
        lim[2] = new Vector3(-tam, tam, -tam);
        lim[3] = new Vector3(-tam, tam, tam);
        lim[4] = new Vector3(tam, -tam, -tam);
        lim[5] = new Vector3(tam, -tam, tam);
        lim[6] = new Vector3(tam, tam, -tam);
        lim[7] = new Vector3(tam, tam, tam);

        planes[(int)PLANES.TOP]   = new Plane(lim[1], lim[3], lim[7]);
        planes[(int)PLANES.BOT]   = new Plane(lim[0], lim[2], lim[6]);
        planes[(int)PLANES.FRONT] = new Plane(lim[0], lim[4], lim[5]);
        planes[(int)PLANES.BACK]  = new Plane(lim[2], lim[3], lim[7]);
        planes[(int)PLANES.LEFT]  = new Plane(lim[0], lim[2], lim[3]);
        planes[(int)PLANES.RIGHT] = new Plane(lim[5], lim[7], lim[6]);

        planes[1].Flip(); //Correccion para que todos los planos miren al centro
        planes[2].Flip();
        planes[3].Flip();

        mesh = col.GetComponent<MeshFilter>();

    }

    private bool shown = false;
    
    void Update()
    {
        //List<Vector3> vertex = new List<Vector3>();
        edges = mesh.sharedMesh.vertices;
        //mesh.mesh.GetVertices(vertex);

        for (int i = 0; i < edges.Length; i++)
        {
            edges[i] = col.transform.TransformPoint(edges[i]);
        }

        DrawColliderBoxLines();
        DrawPlaneBoxLines();

        bool atLeastOneInside = false;
        
        foreach (Vector3 edge in edges) //Ni idea por qué pongo edges, si realmente son vértices 
        {
            bool isInside = true;
            foreach (Plane plane in planes)
            {

                if (isInside)
                {
                    //Debug.Log(point.ToString());
                    isInside = plane.GetSide(edge);
                    
                }
            }
            if (isInside)
            {
                atLeastOneInside = true;
            }
        }

        

        if (atLeastOneInside)
        {
            if (!shown)
            {
                //shown = true;
                Debug.Log("El Cubo esta Adentro");
            }
        }
        else
        {
            Debug.Log("afuera");
        }

    }

    void DrawPlaneBoxLines()
    {
        for (int i = 0; i < lim.Length; i++)
        {
            for (int j = 0; j < lim.Length; j++)
            {
                Debug.DrawLine(lim[i], lim[j], Color.green);

            }
        }
    }

    void DrawColliderBoxLines()
    {
        for (int i = 0; i < edges.Length; i++)
        {
            for (int j = 0; j < edges.Length; j++)
            {
                Debug.DrawLine(edges[i], edges[j], Color.red);
            }
        }
    }

}
